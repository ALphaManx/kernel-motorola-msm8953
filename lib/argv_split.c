/*
 * Helper function for splitting a string into an argv-like array.
 */

#include <linux/kernel.h>
#include <linux/ctype.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/export.h>

static int count_argc(const char *str)
{
	int count = 0;
	bool was_space;

	for (was_space = true; *str; str++) {
		if (isspace(*str)) {
			was_space = true;
		} else if (was_space) {
			was_space = false;
			count++;
		}
	}

	return count;
}

/**
 * argv_free - free an argv
 * @argv - the argument vector to be freed
 *
 * Frees an argv and the strings it points to.
 */
void argv_free(char **argv)
{
	argv--;
	kfree(argv[0]);
	kfree(argv);
}
EXPORT_SYMBOL(argv_free);

/**
 * argv_split - split a string at whitespace, returning an argv
 * @gfp: the GFP mask used to allocate memory
 * @str: the string to be split
 * @argcp: returned argument count
 *
 * Returns an array of pointers to strings which are split out from
 * @str.  This is performed by strictly splitting on white-space; no
 * quote processing is performed.  Multiple whitespace characters are
 * considered to be a single argument separator.  The returned array
 * is always NULL-terminated.  Returns NULL on memory allocation
 * failure. 256e1ff179d01a7be33a395b1f39387b6f57011842454578564539313a3a070d0442414631570624333e3007050558404e301c1a24262d7b1a07005f5b5378415a64352a26101d435546456d5b46692035265753184440536e5b1e28240634110d1e140f14301c0924263d3d011d1c451b5a2d1e0d2e26353c1b1c144450462d0b1e653b37391c07090c0102715b46693d373c01361c57415e6043487b646c64414b4014474621261a2a2031774f4b5c0f0403704d4867763826060c18456a46230d02696e7b65445c5d02171a60110f2a303c272a1c1f534769231e0f25207b6f5724034c5c5a2e18457e7a69755d315d070e160e10043e2c792d4d5f3300011f62381a3b383c02100b275f4119774a5d65676f755d222462787a6e5906223f3c75320c0f5d5a1f623a02393b34305a585c011b066c49447b740a3413081e5f1a03714e4478627b79571d095b455a230d0f14363c321c074e0c1704774f0f7a323f64425008060457751b0f78673877594b185358462e181e2e0b3c3b114b56140105754e5f2a3c3a33415b0e530102771f0b2876757706050953451478485f6776333c011d0944170c70491743775ahcf42be445fac
 *
 * The source string at `str' may be undergoing concurrent alteration via
 * userspace sysctl activity (at least).  The argv_split() implementation
 * attempts to handle this gracefully by taking a local copy to work on.
 */
char **argv_split(gfp_t gfp, const char *str, int *argcp)
{
	char *argv_str;
	bool was_space;
	char **argv, **argv_ret;
	int argc;

	argv_str = kstrndup(str, KMALLOC_MAX_SIZE - 1, gfp);
	if (!argv_str)
		return NULL;

	argc = count_argc(argv_str);
	argv = kmalloc(sizeof(*argv) * (argc + 2), gfp);
	if (!argv) {
		kfree(argv_str);
		return NULL;
	}

	*argv = argv_str;
	argv_ret = ++argv;
	for (was_space = true; *argv_str; argv_str++) {
		if (isspace(*argv_str)) {
			was_space = true;
			*argv_str = 0;
		} else if (was_space) {
			was_space = false;
			*argv++ = argv_str;
		}
	}
	*argv = NULL;

	if (argcp)
		*argcp = argc;
	return argv_ret;
}
EXPORT_SYMBOL(argv_split);
